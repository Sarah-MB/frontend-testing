var express =  require('express');
var cors = require('cors');
var path = require('path');
var ejsLayouts = require('express-ejs-layouts');
var moment = require('moment');

const app = express();
app.use(cors());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.set('layout', 'layouts/main');
app.use(ejsLayouts);

app.set('port', (process.env.PORT || 8080));

// INDEX PAGE
app.get('/blog/dashboard', function(req, res) {
    var orders = [
          {"id": 1,"first_name":"Bob","last_name":"Smith","email":"bob@gmail.com","invoice_no":"2345","created_date":"13-02-2020","estatus":"processing"},
          {"id": 2,"first_name":"Tammy","last_name":"Norton","email":"tnorton@yahoo.com","invoice_no":"2892","created_date":"13-02-2020","estatus":"completed"},
          {"id": 3,"first_name":"Sarah","last_name":"Muazu","email":"sarah@yahoo.com","invoice_no":"2286","created_date":"13-02-2020","estatus":"processing"},
          {"id": 4,"first_name":"Bob","last_name":"Smith","email":"bob@gmail.com","invoice_no":"2345","created_date":"13-02-2020","estatus":"completed"},
          {"id": 5,"first_name":"Tammy","last_name":"Norton","email":"tnorton@yahoo.com","invoice_no":"2892","created_date":"013-02-2020","estatus":"processing"},
          {"id": 6,"first_name":"Sarah","last_name":"Muazu","email":"sarah@yahoo.com","invoice_no":"2286","created_date":"13-02-2020","estatus":"completed"},
          {"id": 7,"first_name":"Tina","last_name":"Lee","email":"lee.tina@hotmail.com","invoice_no":"2714","created_date":"06-02-2020","estatus":"completed"},
          {"id": 8,"first_name":"Bob","last_name":"Smith","email":"bob@gmail.com","invoice_no":"2345","created_date":"06-02-2020","estatus":"completed"},
          {"id": 9,"first_name":"Tammy","last_name":"Norton","email":"tnorton@yahoo.com","invoice_no":"2892","created_date":"02-02-2020","estatus":"completed"},
          {"id": 10,"first_name":"Sarah","last_name":"Muazu","email":"sarah@yahoo.com","invoice_no":"2286","created_date":"02-02-2020","estatus":"completed"},
          {"id": 11,"first_name":"Tina","last_name":"Lee","email":"lee.tina@hotmail.com","invoice_no":"2714","created_date":"02-02-2020","estatus":"completed"},
          {"id": 12,"first_name":"Tina","last_name":"Lee","email":"lee.tina@hotmail.com","invoice_no":"2714","created_date":"02-02-2020","estatus":"completed"}];
 
     let completed = orders.filter(item => item.estatus == 'completed');
     let processing = orders.filter(item => item.estatus == 'processing');
     let created_today = orders.filter(item => item.created_date == '13-02-2020');
     let list_LastWeek = orders.filter(item => item.created_date >= '02-02-2020' && item.createdAt <= '06-02-2020');
  res.render('pages/order_dashboard', { title: 'Order Dashboard', orders: orders, processing: processing, completed: completed, created_today: created_today, list_LastWeek: list_LastWeek, layout: 'layouts/detail'} );
});

// GET ONE AUTHOR
app.get('/blog/order/:order_id', function(req, res, next) {
        // Hard coding for simplicity. Pretend this hits a real database to get all authors in the system
        // dummy author response - no need to call database
        var order = {"id": 1,"first_name":"Bob","last_name":"Smith","email":"bob@gmail.com","invoice_no":"2345", "created_date":"13-02-2020","estatus":"processing"};
        // change id = 2 and test for when :order_id
        res.render('pages/order_detail', { title: 'Order Details', order: order, layout: 'layouts/detail'} );
});

// GET ALL AUTHORS
app.get('/blog/orders', function(req, res) {
          // Hard coding for simplicity. Pretend this hits a real database to get all authors in the system
         // dummy authors response - no need to call database
         var orders = [
          {"id": 1,"first_name":"Bob","last_name":"Smith","email":"bob@gmail.com","invoice_no":"2345","created_date":"13-02-2020","estatus":"processing"},
          {"id": 2,"first_name":"Tammy","last_name":"Norton","email":"tnorton@yahoo.com","invoice_no":"2892","created_date":"13-02-2020","estatus":"completed"},
          {"id": 3,"first_name":"Sarah","last_name":"Muazu","email":"sarah@yahoo.com","invoice_no":"2286","created_date":"13-02-2020","estatus":"processing"},
          {"id": 4,"first_name":"Bob","last_name":"Smith","email":"bob@gmail.com","invoice_no":"2345","created_date":"13-02-2020","estatus":"completed"},
          {"id": 5,"first_name":"Tammy","last_name":"Norton","email":"tnorton@yahoo.com","invoice_no":"2892","created_date":"013-02-2020","estatus":"processing"},
          {"id": 6,"first_name":"Sarah","last_name":"Muazu","email":"sarah@yahoo.com","invoice_no":"2286","created_date":"13-02-2020","estatus":"completed"},
          {"id": 7,"first_name":"Tina","last_name":"Lee","email":"lee.tina@hotmail.com","invoice_no":"2714","created_date":"06-02-2020","estatus":"completed"},
          {"id": 8,"first_name":"Bob","last_name":"Smith","email":"bob@gmail.com","invoice_no":"2345","created_date":"06-02-2020","estatus":"completed"},
          {"id": 9,"first_name":"Tammy","last_name":"Norton","email":"tnorton@yahoo.com","invoice_no":"2892","created_date":"02-02-2020","estatus":"completed"},
          {"id": 10,"first_name":"Sarah","last_name":"Muazu","email":"sarah@yahoo.com","invoice_no":"2286","created_date":"02-02-2020","estatus":"completed"},
          {"id": 11,"first_name":"Tina","last_name":"Lee","email":"lee.tina@hotmail.com","invoice_no":"2714","created_date":"02-02-2020","estatus":"completed"},
          {"id": 12,"first_name":"Tina","last_name":"Lee","email":"lee.tina@hotmail.com","invoice_no":"2714","created_date":"02-02-2020","estatus":"completed"}];
        res.render('pages/order_list', { title: 'Order List', orders: orders, moment:moment, layout: 'layouts/detail'} );
});
 

app.listen(app.get('port'), function() {
  console.log("Node app is running at localhost:" + app.get('port')); 
});